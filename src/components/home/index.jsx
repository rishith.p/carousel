import React, { useState } from 'react';

// import GameBoard from '../game-board';
import Carousel from '../carousel';

import './styles/index.scss';

const Home = () => {
    return (<div className="container">
		  <Carousel />
    </div>)
}

export default Home;