import React, { useState } from 'react';

const Carousel = ({images}) => {
    const [currentImageIndex, setCurrentImageIndex] = useState(0);
    
    const renderImageStrip = () => {
        let indexesToRender = [
            ...getBackwardIndexes(currentImageIndex, images.length, 2), 
            currentImageIndex, 
            ...getForwardIndexes(currentImageIndex, images.length, 2)
        ];

        return <div className="image-strip">
            {React.Children.toArray(indexesToRender.map(indexToRender => <img src={images[indexToRender]} width={40} height={40}/>))}
        </div>
    }

    const moveBackward = () => {
        setCurrentImageIndex(index - 1 < 0 ? images.length - 1 : index - 1);
    }

    const moveForward = () => {
        setCurrentImageIndex((index + 1) % images.length);
    }

    const getBackwardIndexes = (currentIndex, length, noOfElements) => {
        let indexes = [];
        let index = currentIndex;
        while(indexes.length < noOfElements) {
            index = index - 1;
            if(index < 0) {
                index = length - 1;
            }
            indexes.push(index);
        }

        return indexes;
    }

    const getForwardIndexes = (currentIndex, length) => {
        let indexes = [];
        let index = currentIndex;
        while(indexes.length < noOfElements) {
            index = (index + 1) % length;
            indexes.push(index);
        }

        return indexes;
    }

    return <div className="container">
        <button onClick={moveBackward}>{"<"}</button>
        <img className="jumbotron" src={images[currentImageIndex]} width={600} height={400}/>
        <button onClick={moveForward}>{">"}</button>
        {renderImageStrip()}
    </div>
}

export default Carousel;