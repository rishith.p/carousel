import { random } from 'lodash';
import React, { useEffect, useRef, useState } from 'react';

import { getBoardMatrix, getPlayerPieces, getWinner} from '../../utils/game-board-utils';

import './styles/styles.scss';

const GameBoard = ({
    playerChars = [],
    dimensionX = 3,
    dimensionY = 3,
    noOfPlayers = 2,
    charactersToChooseFrom
}) => {
    let currentPlayer = 0;
    let boardRef = useRef();

    const [boardMatrix, setBoardMatrix] = useState([]);
    const [playerPieces, setPlayerPieces] = useState([]);

    useEffect(() => {
        if(playerChars.length !== noOfPlayers) {
            setPlayerPieces(getPlayerPieces(noOfPlayers, charactersToChooseFrom)); 
        } else {
            setPlayerPieces(playerChars);
        }

        setBoardMatrix(getBoardMatrix(dimensionX, dimensionY));
    }, [])

    const onPlay = event => {
        const target = event.target;
        target.innerHTML = playerPieces[currentPlayer];
        currentPlayer = (currentPlayer + 1) % noOfPlayers;
        getWinner(boardRef);
    }

    const renderBoard = boardMatrix => <table className="game-board">
        <tbody onClick={onPlay} className="body" ref={boardRef}>
            {React.Children.toArray(boardMatrix.map(row => <tr className="row">
                {React.Children.toArray(row.map(column => <td className="td" data-id={column}></td>))}
            </tr>))}
        </tbody>
    </table>

    return <div className="container">
        {renderBoard(boardMatrix)}
    </div>
}

export default GameBoard;