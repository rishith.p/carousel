export const getBoardMatrix = (dimensionX, dimensionY) => {
    const matrix = [];
    let counter = 1;
    for(let i=0; i<dimensionY; i++) {
        const row = [];

        for(let j=0; j<dimensionX; j++) {
            row.push(counter++);
        }

        matrix.push(row);
    }
    return matrix;
}

const getRandomCharacter = (characters) => {
    if(!!!characters) {
        characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    }
    const charactersLength = characters.length;
    return characters.charAt(Math.floor(Math.random() * charactersLength));
}

export const getPlayerPieces = (noOfPlayers, characters) => {
    const playerPieces = new Set();
    while(playerPieces.size !== noOfPlayers) {
        const char = getRandomCharacter(characters);
        playerPieces.add(char);
    }

    return [...playerPieces];
}

export const getWinner = (boardRef) => {
    // const board = [];
    // [...boardRef.current.children].forEach(row => {
    //     const row = [];
    //     [...row.children].forEach(cell => )
    // })
}